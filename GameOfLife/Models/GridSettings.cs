﻿namespace GameOfLife.Models
{
    public class GridSettings
    {
        public int Width;
        public int Length;
        public int PercentOfLifeCells;
    }
}
