﻿using GameOfLife.Interfaces;
using GameOfLife.Models;
using System;

namespace GameOfLife.Validators
{
    public class GridSettingsValidator : IValidator
    {
        private GridSettings _settings;

        public GridSettingsValidator(GridSettings settings)
        {
            _settings = settings;
        }

        public void Validate()
        {
            if (_settings == null)
            {
                throw new Exception("Settings can not be null");
            }
            if (_settings.Width <= 0)
            {
                throw new Exception("Grid settings Width should be at least 1");
            }
            if (_settings.Length <= 0)
            {
                throw new Exception("Grid settings Length should be at least 1");
            }
            if (_settings.PercentOfLifeCells < 1 || _settings.PercentOfLifeCells > 100)
            {
                throw new Exception("Grid settings PercentOfLifeCells should be between 1 and 100");
            }
        }
    }
}
