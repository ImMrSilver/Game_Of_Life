﻿namespace GameOfLife.Interfaces
{
    public interface IValidator
    {
        void Validate();
    }

}
