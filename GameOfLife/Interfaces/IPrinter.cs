﻿namespace GameOfLife.Interfaces
{
    public interface IPrinter
    {
        void Print(int[][] grid);
    }
}
