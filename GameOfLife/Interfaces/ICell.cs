﻿namespace GameOfLife.Interfaces
{
    public interface ICell
    {
          int SetCellValue(int IsAlive, int LifeCellCount);
    }
}
