﻿using GameOfLife.Models;

namespace GameOfLife.Interfaces
{
    public interface IGrid
    {
        int[][] Get();

        int[][] CreateInitialGrid(GridSettings settings);

        int[][] CreateCustomGrid(int[][] customGrid);

        int[][] Update();

        bool Equals(int[][] grid, int[][] updatedGrid);
    }
}
