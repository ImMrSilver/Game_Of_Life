﻿namespace GameOfLife.Business
{
    public static class Cell
    {
        public static int SetCellValue(int isAlive, int LifeCellCount)
        {
            switch (isAlive)
            {
                case 1:
                    return SetLifeCellValue(LifeCellCount);
                case 0:
                    return SetDeadCellValue(LifeCellCount);
                default:
                    return 0;
            }
        }

        private static int SetLifeCellValue(int LifeCellCount)
        {
            if (LifeCellCount <= 1)
            {
                return 0;
            }
            if (LifeCellCount == 2 || LifeCellCount == 3)
            {
                return 1;
            }
            return 0;
        }

        private static int SetDeadCellValue(int LifeCellCount)
        {
            if (LifeCellCount == 3)
            {
                return 1;
            }
            return 0;
        }
    }
}
