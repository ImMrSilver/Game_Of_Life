﻿using GameOfLife.Interfaces;

namespace GameOfLife.Business
{
    public class Gameplay: IGameplay
    {
        private IPrinter _printer;
        private IGrid _grid;

        private Gameplay()
        {
        }

        public Gameplay (IPrinter printer, IGrid grid)
        {
            _printer = printer;
            _grid = grid;
        }

        public void Start(int[][] grid)
        {
            var updatedGrid = _grid.Update();
            var gridsAreNotEqual = !_grid.Equals(grid, updatedGrid);
            if (gridsAreNotEqual)
            {
                _printer.Print(updatedGrid);
                Start(updatedGrid);
            }
        }
    }
}
