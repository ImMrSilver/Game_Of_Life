﻿using GameOfLife.Interfaces;
using System;

namespace GameOfLife.Business
{
    public class GridPrinter : IPrinter
    {
        public void Print(int[][] grid)
        {
            var border = new String('-', grid[0].Length + 2);
            Console.WriteLine(border);
            for (int y = 0; y < grid.Length; y++)
            {
                Console.Write("|");
                for (int x = 0; x < grid[0].Length; x++)
                {
                    Console.Write(grid[y][x] == 1 ? "*" : " ");
                }
                Console.Write("|");
                Console.WriteLine();
            }
            Console.WriteLine(border);
        }
    }
}
