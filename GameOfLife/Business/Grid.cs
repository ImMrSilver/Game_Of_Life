﻿using GameOfLife.Interfaces;
using GameOfLife.Models;
using GameOfLife.Validators;
using System;
using System.Linq;

namespace GameOfLife.Business
{
    public class Grid : IGrid
    {
        private int[][] _grid;

        public Grid(GridSettings settings)
        {
            CreateInitialGrid(settings);
        }

        public int[][] Get()
        {
            return _grid;
        }

        public int[][] CreateInitialGrid(GridSettings settings)
        {
            IValidator _gridSettingsValidator = new GridSettingsValidator(settings);
            _gridSettingsValidator.Validate();
            _grid = FillGridWithInitialValues(settings);
            return _grid;
        }

        public int[][] Update()
        {
            var updatedGrid = new int[_grid.Length][];
            for (var i = 0; i < _grid.Length; i++)
            {
                for (var j = 0; j < _grid[i].Length; j++)
                {
                    if (j == 0)
                    {
                        updatedGrid[i] = new int[_grid[i].Length];
                    }

                    var lifeCellCount = GetLifeCellNeighbourCount(i, j);
                    var isAlive = _grid[i][j];
                    updatedGrid[i][j] = Cell.SetCellValue(isAlive,lifeCellCount);
                }
            }
            _grid = updatedGrid;
            return _grid;
        }

        #region GetLifeCellCount
        private int GetLifeCellNeighbourCount(int i,int j)
        {
            var LifeCellCount = 0;
            //Update LifeCellCount for the row of cells above the current one
            if (i - 1 >= 0)
            {
                LifeCellCount +=  LifeCellCountInRow(i - 1, j);
            }

            // Update LifeCellCount in the same row as the current one
            LifeCellCount += LifeCellCountInRow(i, j, false);

            //Update LifeCellCount for the row of cells below the current one
            if (i + 1 < _grid.Length)
            {
                LifeCellCount += LifeCellCountInRow(i + 1, j);
            }

            return LifeCellCount;
        }

        private int LifeCellCountInRow(int i, int j, bool includeMiddleCell = true)
        {
            var LifeCellCount = 0;
            //(i-1; j-1)
            if (j - 1 >= 0)
            {
                LifeCellCount += _grid[i][j - 1];
            }
            //(i-1; j)
            if (includeMiddleCell)
            {
                LifeCellCount += _grid[i][j];
            }
            //(i-1;j+1)
            if (j + 1 < _grid[i].Length)
            {
                LifeCellCount += _grid[i][j + 1];
            }
            return LifeCellCount;
        }
        #endregion

        public bool Equals(int[][] grid, int[][] updatedGrid)
        {
            if (grid.Length == updatedGrid.Length)
            {
                for (int i = 0; i < grid.Length; i++)
                {
                    if (grid[i].Length == updatedGrid[i].Length)
                    {
                        if (!grid[i].SequenceEqual(updatedGrid[i]))
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }
            return true;
        }

        private int[][] FillGridWithInitialValues(GridSettings settings)
        {
            int[][] grid = new int[settings.Length][];
            Random rnd = new Random();
            for (var i = 0; i < settings.Length; i++)
            {
                for (var j = 0; j < settings.Width; j++)
                {
                    if (j == 0)
                    {
                        grid[i] = new int[settings.Width];
                    }
                    var randomNumber = rnd.Next(1, 10);
                    var isAlive = randomNumber <= (settings.PercentOfLifeCells / 10);
                    grid[i][j] = isAlive ? 1 : 0;
                }
            }
            return grid;
        }
        
        //For tests only, to test custom grid state changes
        public int[][] CreateCustomGrid(int[][] customGrid)
        {
            _grid = customGrid;
            return _grid;
        }
    }
}
