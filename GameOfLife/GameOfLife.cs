﻿using GameOfLife.Business;
using GameOfLife.Interfaces;
using GameOfLife.Models;
using System;

namespace GameOfLife
{
    public class GameOfLife
    {
        public static void Main(string[] args)
        {
            GridSettings settings = new GridSettings()
            {
                Width = 10,
                Length = 10,
                PercentOfLifeCells = 20
            };

            IGrid _grid = new Grid(settings);
            IPrinter _printer = new GridPrinter();
            IGameplay _gameplay = new Gameplay(_printer, _grid);
           
             int[][] initialGrid =_grid.CreateInitialGrid(settings);
            _printer.Print(initialGrid);
            _gameplay.Start(initialGrid);

            Console.WriteLine("Game of life has ended.");
            Console.ReadLine();
        }
    }
}
