﻿using GameOfLife.Business;
using GameOfLife.Interfaces;
using GameOfLife.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class GridAndCellTests
    {
        private static GridSettings _settings = new GridSettings()
        {
            Width = 15,
            Length = 17,
            PercentOfLifeCells = 20
        };
        private static IGrid _grid = new Grid(_settings);

        #region Grid
        [TestMethod]
        public void CreateInitialGrid_ShouldReturnACreatedInitialGrid()
        {
            //arrange
           
            //act
             var grid =_grid.CreateInitialGrid(_settings);
            //assert
            Assert.AreEqual(17, grid.Length);
            Assert.AreEqual(15, grid[0].Length);
        }

        [TestMethod]
        public void UpdateGrid_ShouldReturn_CorrectlyUpdatedGrid()
        {
            //arrange
            int[][] grid =
                            {
                                new[]
                                {
                                   1,1,1
                                },
                                new[]
                                {
                                   1,1,1
                                },
                                new[]
                                {
                                   1,1,1
                                }
                            };
            //act 1 turn
            _grid.CreateCustomGrid(grid);
            var updatedGrid =_grid.Update();
          
            //assert
            Assert.AreEqual(1, updatedGrid[0][0]);
            Assert.AreEqual(1, updatedGrid[0][2]);
            Assert.AreEqual(1, updatedGrid[2][0]);
            Assert.AreEqual(1, updatedGrid[2][2]);

            Assert.AreEqual(0, updatedGrid[0][1]);
            Assert.AreEqual(0, updatedGrid[1][0]);
            Assert.AreEqual(0, updatedGrid[1][1]);
            Assert.AreEqual(0, updatedGrid[1][2]);
            Assert.AreEqual(0, updatedGrid[2][1]);

            //act 2 turn
            updatedGrid = _grid.Update();
        
            //assert
            Assert.AreEqual(0, updatedGrid[0][0]);
            Assert.AreEqual(0, updatedGrid[0][2]);
            Assert.AreEqual(0, updatedGrid[2][0]);
            Assert.AreEqual(0, updatedGrid[2][2]);
            Assert.AreEqual(0, updatedGrid[0][1]);
            Assert.AreEqual(0, updatedGrid[1][0]);
            Assert.AreEqual(0, updatedGrid[1][1]);
            Assert.AreEqual(0, updatedGrid[1][2]);
            Assert.AreEqual(0, updatedGrid[2][1]);
        }

        [TestMethod]
        public void GridsAreEqual_ShouldReturnFalse_WhenTwoGridsAreNotEqual()
        {
            //arrange
            int[][] grid =
                            {
                                new[]
                                {
                                   1,1,1
                                },
                                new[]
                                {
                                   1,1,1
                                },
                                new[]
                                {
                                   1,1,1
                                }
                            };
            _grid.CreateCustomGrid(grid);
            var updatedGrid = _grid.Update();
           
            //act
            var gridsArenotEqual = _grid.Equals(grid, updatedGrid);
            //assert
            Assert.AreEqual(false, gridsArenotEqual);
        }
        #endregion

        #region SetCellValue
        /*I'd like to have here a single test with 5 test scenarios, but I don't think
          DataRow can use int[][] as parameter.
          So we should have a single test with 5 cases instead of 5 tests*/
        [TestMethod]
        public void SetCellValue_ShouldReturn_0_IfThereAreNoNeighbours()
        {
            //arrange
            int[][] grid =
                            {
                                new[]
                                {
                                   0,0,0
                                },
                                new[]
                                {
                                   0,1,0
                                },
                                new[]
                                {
                                   0,0,0
                                }
                            };
            //act
            var cellValue = Cell.SetCellValue(1, 0);
            //assert
            Assert.AreEqual(0, cellValue);
        }
        [TestMethod]
        public void SetCellValue_ShouldReturn_0_IfThereAre_4_OrMoreNeighbours()
        {
            //arrange
            int[][] grid =
                            {
                                new[]
                                {
                                   0,0,1
                                },
                                new[]
                                {
                                   1,1,1
                                },
                                new[]
                                {
                                   1,0,0
                                }
                            };
            //act
            var cellValue =  Cell.SetCellValue(1, 4);
            //assert
            Assert.AreEqual(0, cellValue);
        }
        [TestMethod]
        public void SetCellValue_ShouldReturn_1_IfThereAre_2_Or_3_Neighbours()
        {
            //arrange
            int[][] grid =
                            {
                                new[]
                                {
                                   0,0,0
                                },
                                new[]
                                {
                                   1,1,1
                                },
                                new[]
                                {
                                   0,0,0
                                }
                            };
            //act
            var cellValue = Cell.SetCellValue(1, 2);
            //assert
            Assert.AreEqual(1, cellValue);
        }
        [TestMethod]
        public void SetCellValue_ShouldReturn_1_IfThereAre_3_Life_Neighbours()
        {
            //arrange
            int[][] grid =
                            {
                                new[]
                                {
                                   1,1,1
                                },
                                new[]
                                {
                                   0,0,0
                                },
                                new[]
                                {
                                   0,0,0
                                }
                            };
            //act
            var cellValue = Cell.SetCellValue(0, 3);
            //assert
            Assert.AreEqual(1, cellValue);
        }
        [TestMethod]
        public void SetCellValue_ShouldReturn_1_IfItIsInTheCornetAndThereAre_3_Life_Neighbours()
        {
            //arrange
            int[][] grid =
                            {
                                new[]
                                {
                                   1,1,0
                                },
                                new[]
                                {
                                   1,1,0
                                },
                                new[]
                                {
                                   0,0,0
                                }
                            };
            //act
            var cellValue = Cell.SetCellValue(1, 3);
            //assert
            Assert.AreEqual(1, cellValue);
        }
        #endregion
       
    }
}

