﻿using System;
using GameOfLife.Interfaces;
using GameOfLife.Models;
using GameOfLife.Validators;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class GridSettingsValidatorTests
    {
        IValidator _validator;

        [TestMethod]
        [ExpectedException(typeof(Exception), "Settings can not be null")]
        public void ShouldThrowExceptionWhenSettingsAreNull()
        {
            _validator = new GridSettingsValidator(null);
            _validator.Validate();
        }
        [TestMethod]
        [ExpectedException(typeof(Exception), "Grid settings Width should be at least 1")]
        public void ShouldThrowExceptionWhenSettingsWidthIsBelow1()
        {
            var settings = new GridSettings
            {
                Width = 0
            };
            _validator = new GridSettingsValidator(settings);
            _validator.Validate();
        }
        [TestMethod]
        [ExpectedException(typeof(Exception), "Grid settings Length should be at least 1")]
        public void ShouldThrowExceptionWhenSettingsLengthIsBelow1()
        {
            var settings = new GridSettings
            {
                Width = 1,
                Length=0
            };
            _validator = new GridSettingsValidator(settings);
            _validator.Validate();
        }
        [TestMethod]
        [ExpectedException(typeof(Exception), "Grid settings PercentOfLifeCells should be between 1 and 100")]
        public void ShouldThrowExceptionWhenSettingsPercentageIsAbove100()
        {
            var settings = new GridSettings
            {
                Width = 1,
                Length = 1,
                PercentOfLifeCells=250
            };
            _validator = new GridSettingsValidator(settings);
            _validator.Validate();
        }
    }
}
